package TaskOne;

import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public String concatenateWords(String s1, String s2) throws NonLatinException {
        String regexp = "[\\w]*";
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcherS1 = pattern.matcher(s1);
        Matcher matcherS2 = pattern.matcher(s2);
        if (matcherS1.matches() && matcherS2.matches())
            return s1.concat(s2);
        throw new NonLatinException();
    }

    public BigInteger computeFactorial(int n) {
        if (n < 0) throw new IllegalArgumentException();
        if (n == 0)
            return BigInteger.valueOf(1);
        return new BigInteger(Long.toString(n)).multiply(computeFactorial(n - 1));
    }
}
