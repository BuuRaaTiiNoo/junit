package TaskTwo;

import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class ComputeTest {


    private Compute compute = new Compute();

    @Test(expected = IllegalArgumentException.class)
    public void calculateDivByZero() {
        String exp = "-1/0";
        String result = compute.calculate(exp);
    }
    @Test
    public void calculateSub() {
        String exp = "1-1";
        String result = compute.calculate(exp);
        assertThat("0.0",equalTo(result));
    }
    @Test
    public void calculateAdd() {
        String exp = "1+1";
        String result = compute.calculate(exp);
        assertEquals("2.0", result);
    }
    @Test
    public void calculateDiv() {
        String exp = "2/2";
        String result = compute.calculate(exp);
        assertEquals("1.0", result);
    }
    @Test
    public void calculateMult() {
        String exp = "2*1";
        String result = compute.calculate(exp);
        assertEquals("2.0", result);
    }

    @Test
    public void calculateNotValidationExp() {
        String exp = "2**1";
        String result = compute.calculate(exp);
        assertEquals("expression " + exp + " incorrect", result);
    }
}